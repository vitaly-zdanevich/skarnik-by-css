Black night theme for skarnik.by

<div align='center'>
    <a href='https://gitlab.com/vitaly-zdanevich/skarnik-by-css/-/raw/master/skarnik-by.user.css' alt='Install with Stylus'>
        <img src='https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=flat' />
    </a>
</div>

![screenshot](/screenshot.png)

For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us). Mirroring to https://userstyles.world/style/14999/skarnik-by-black-night
